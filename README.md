# kraken.io.im

Observation of pictures, optimization kraken.io

# Install

`yarn install` or `npm i`

# Use

--mask, -m - Mask for filtered (includes function). String: 'filename1, filename2, filename3'

--from, -f - Where to find the source and where to put them back. String: '/path1, /path2, /path3'

--is-finished - Flag: Stop the program after the picture optimization

--part,-p - The number of elements in the party Default: 10

--delay,-d - Time ( ms ) delay for restart  Default: 3600000

--verbose  - Default: false

```sh
$ node jndex.js

$ node jndex.js -f ./patch/to_images --verbose --is-finished

$ node jndex.js -f './patch/to_images, ./patch1/to_images'

$ node jndex.js -f './patch/to_images, ./patch1/to_images' --mask 'preview.*, patch1/origin*, *.png'
```

> NOTE: If the mask contains a '/', then check the current dirrektoriya. If a directory is not suited to the mask, the file will go to the processing otherwise check the file name on the mask

> EXAMPLE: 'preview.*' - checks on the mask anywhere, 'patch1/origin.*' - check the mask only in path 'patch1' and 'origin *' mask to another directory without checking.

Config file example: https://kraken.io/docs/generating-image-sets and add sample keys: "logger", "from"...


```json
{
    "auch": {
        "api_key": "your-api-key",
        "api_secret": "your-api-secret"
    },
    "from": "/patch/to_images/...",
    "logger": {
		'logDirectory': './',
		'fileNamePattern': 'kraken.io-<DATE>.log'
	}
}
```

> NOTE: Default name: 'kraken.io.auch.json'

`node jndex.js -c ./kraken.io.auch.json`


