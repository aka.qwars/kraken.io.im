function iter$(a){ return a ? (a.toArray ? a.toArray() : a) : []; };
var fs = require('fs');
var request = require('request');
var isImage = require('image-type');
var readChunk = require('read-chunk');
var moment = require('moment');
var pnexif = require('png-metadata');
var piexif = require('piexifjs');
var modifyExif = require('modify-exif');
var wildcard = require('wildcard');

var option = {
	'wait': true,
	'lossy': true,
	'part': 10,
	'delay': 3000,
	'logger': {
		'logDirectory': './',
		'fileNamePattern': 'kraken.io-<DATE>.log'
	}
};

var Argv = new Map((function() {
	var res = [];
	for (var i = 0, items = iter$(process.argv), len = items.length; i < len; i++) {
		res.push([items[i],(process.argv[i + 1] || '').match(/^-+/) ? undefined : process.argv[i + 1]]);
	};
	return res;
})());


var help = function(message) {
	if (message) { console.log(message) };
	console.log("");
	console.log("--config, -c - The configuration file and access to the service of optimization of key kraken.io. Default: ./kraken.io.auch.json");
	console.log("--mask, -m - Mask for filtered (includes function). String: 'filename1, filename2, filename3'");
	console.log("--from, -f - Where to find the source and where to put them back. String: '/path1, /path2, /path3'");
	console.log("--is-finished - Flag: Stop the program after the picture optimization");
	console.log("--part,-p - The number of elements in the party Default: 10");
	console.log("--delay,-d - Time ( ms ) delay for restart  Default: 3600000");
	console.log("--verbose  - Default: false");
	console.log("");
	return process.exit();
};


Argv.get('--help') || Argv.get('-h') && help();

var keyKrakenAuch = Argv.get("--config") || Argv.get("-c") || './kraken.io.auch.json';;

if (!fs.statSync(("" + (process.env.PWD) + "/" + keyKrakenAuch))) { help(("Not exists config data API file: " + keyKrakenAuch)) };

Object.assign(option,require(("" + (process.env.PWD) + "/" + keyKrakenAuch)));

if (!option.auth) { help("BAD exists param --config : auth") };

if (!option.hasOwnProperty('finished')) { option.finished = Argv.has("--is-finished") };
if (!option.hasOwnProperty('verbose')) { option.verbose = Argv.has("--verbose") };

option.from = function(_0) { return _0.split(',').map(function(item) { return item.trim().replace(/\/$/,""); }); }(Argv.get("--from") || Argv.get("-f") || option.from || help("Not exists param --from"));
option.mask = function(_0) { return !!_0 && _0.split(',').map(function(item) { return item.trim().replace(/\/$/,""); }); }(Argv.get("--mask") || Argv.get("-m") || option.mask);
option.part = Argv.get("--part") || Argv.get("-p") || option.part;
option.delay = Argv.get("--delay") || Argv.get("-d") || option.delay;

var log = require('simple-node-logger').createRollingFileLogger(option.logger);
log.pathfromitem = [];;

var Kraken = function(_0) { return new _0(option.auth); }(require('kraken'));

/*

	@description Checks - whether optimizing the file was
	@param{ STRING } file
	@returns{ BOOLEAN }

*/

var getDataKraken = function(file) {
	var text = {
		data: ''
	};
	try {
		if (file.includes('.png')) { text = pnexif.splitChunk(pnexif.readFileSync(file)).filter(function(item) { return item.type == 'iTXt'; })[0] || text } else {
			text.data = piexif.dump(piexif.load(fs.readFileSync(file).toString('binary')));
		};
	} catch (e) {
		log.error(("" + (e.message) + ": " + file));
	};
	return text.data.includes('kraken.io');
};

/*

	@description It creates a exif-data file
	@param{ STRING } file File path
	@param{ BOOLEAN } keep Keep date time or not

*/

var createDataKraken = function(file,keep) {
    try {
	if (file.includes('.png')) {
		var list = pnexif.splitChunk(pnexif.readFileSync(file));
		var iend = list.pop();
		list.push(pnexif.createChunk('iTXt',"Modify kraken.io"));
		list.push(iend);
		return fs.writeFileSync(file,pnexif.joinChunk(list),'binary');
	} else {
		return fs.writeFileSync(file,modifyExif(
			fs.readFileSync(file),
			function(data) { return data['0th']['271'] = 'Modify kraken.io'; },
			{keepDateTime: keep}
		));
	};
    }
    catch (e) {
	log.error(("" + (e.message) + ": " + file));
    };
    
};

/*

	@description Check API user data
	@returns{ Promise }

*/

var krakenUserStatus = function() { return new Promise(function(resolve,reject) {
	// Смотрим статус пользователя ключа доступа. Если доступ разрешен сигналим.
	return Kraken.userStatus(function(err,response) {
		if (err) { return reject(err.message) } else if (response.quota_remaining <= 0 && (!response.success || !response.active)) { return reject(log.info(("Quota for the optimization of images kraken.io: " + (response.quota_remaining)))) } else {
			return resolve(response && response.active);
		};
	});
}); };

/*

	@description Download file from URL
	@param{ STRING } file File path
	@param{ STRING } uri URL data file
	@returns{ Promise }

*/

var krakenDownloadImage = function(uri,file) { return new Promise(function(resolve,reject) {
	// Качаем из URL нашу картинку, предварительно собрав данные о старой для любопытных.
	if (!uri) { return resolve(log.warn(("Is not valid URI for source: " + file))) } else {
		var statsize = fs.statSync(file).size;
		return request.head(uri,function(err,res,body) {
			return request(uri).pipe(fs.createWriteStream(file)).on('error',reject.bind(this)).on('close',resolve.bind(this,statsize));
		});
	};
}); };

/*

	@description Upload file to kraken.io
	@param{ STRING } path File path
	@param{ OBJECT } type Add upload oprions
	@returns{ Promise }

*/

var krakenUpdateImage = function(path,type) { if(type === undefined) type = {};
return new Promise(function(resolve,reject) {
	// Отравляем картинку кракену, сигналим по прибытию данных
	return Kraken.upload(
		Object.assign({file: path},Object.assign(option,type)),
		function(err,data) { return err ? reject(err) : resolve(data); }
	);
}); };

/*

	@description Data processing of the whole process on the file
	@param{ STRING } file File path
	@returns{ Promise }

*/

var krakenImagesPartGet = function(file) { return new Promise(function(resolve,reject) {
	// Проверяем текущий статус пользователя, а вдруг чего не того, квота кончилась или сети нет...
	// then - Если все хорошо, то принимай нашу картинку на обработку.
	// then - Если все хорошо, то отдавай нашу картинку.
	return krakenUserStatus().catch(function(e) { return resolve(file,log.error(("" + (e.message) + ": " + file))); }).then(function(isOk) { return isOk && krakenUpdateImage(file).catch(function(e) { return resolve(file,log.error(("" + (e.message) + ": " + file))); }).then(function(resource) { return resource && krakenDownloadImage(resource.kraked_url,file).then(function(statsize) { return fs.stat(file,function(err,data) {
		// Все молодцы, всем спасибо, кому надо посмотри. Метим картинку печатью exif. Сигналим
		option.verbose && log.info(("" + file + " : Optimized " + statsize + " -> " + (data.size)));
		createDataKraken(file);
		return resolve(file);
	}); }); }); });
}); };

/*

	@description Pause message

*/

var krakenImagesPartPause = function() { return option.verbose || console.log(("Pause to : " + moment().add(option.delay,'milliseconds').format('dddd, MMMM Do YYYY, h:mm:ss a'))); };


/*

	@description Processing an array of files
	@param{ ARRAY } sources Files
	@returns{ Promise }

*/

var krakenImagesPart = function(sources) { return new Promise(function(resolve,reject) {
	// Пробигаемся по массиву, отправляем на оптимизацию
	if (sources.length) { return sources.forEach(function(item) { return krakenImagesPartGet(item).then(function(image) {
		// После оптимизации сокращаем массив, если больше в массиве ничего нет, сигналим
		sources.splice(sources.indexOf(image),1);
		log.pathfromitem.push(image);
		if (!sources.length) { return resolve(false) };
	}); }) } else {
		return krakenImagesPart.timeout = clearTimeout(krakenImagesPart.timeout) || krakenImagesPartPause(setTimeout(function() {
			return resolve(true);
		},option.delay));
	};
}); };

var whenState = function(item,pathFrom) {
	try {
		return !(item.name.includes('.webp') || item.name.includes('.gif') || item.name.includes('.bmp')) && item.isFile() && isImage(readChunk.sync(("" + pathFrom + "/" + (item.name)),0,12));
	} catch (e) {
		return log.error(("" + (e.message) + ": " + pathFrom + "/" + (item.name)));
	};
};

var maskFilter = function(pathfromitem) {
	var maskTestFilter = function(m) {
		if (!m.includes('/')) { return wildcard(m,pathfromitem.split('/').reverse()[0]) } else {
			m = m.split('/').reverse();
			m = [m[0],m.slice(1).reverse().concat('').join('/')];
			pathfromitem = pathfromitem.split('/').reverse();
			pathfromitem = [pathfromitem[0],pathfromitem.slice(1).reverse().concat('').join('/')];
			if (pathfromitem[1].includes(m[1])) { return wildcard(m[0],pathfromitem[0]) } else {
				return true;
			};
		};
	};
	
	return option.mask.filter(maskTestFilter).length != 0;
};

/*

	@description Collects some of the files to be processed, launches handlers
	@param{ NUMBER } part Count iteration
	@param{ BOOLEAN } state New start or not

*/

var initialPartPictures = function(pathFrom,part,state) {
	var $0 = arguments, j = $0.length;
	if(j < 2) part = 0;
	if (state && option.finished) { process.exit(1) };
	if (!option.verbose) {
		if (part > 1) {
			if (state) { process.stdout.moveCursor(0,-1) };
			if (state) { part -= 1 };
			process.stdout.clearScreenDown();
			process.stdout.cursorTo(0);
			process.stdout.moveCursor(0,-1);
		};
		
		if (part) { console.log(("Finish part: " + part)) };
	};
	
	krakenUserStatus.sources = [];
	
	// Делаем выборку пакета для оптимизации, первые попавшиеся не картинки исключаяя .bmp и .gif
	// Кракен их не принимает
	for (var i1 = 0, ary = iter$(fs.readdirSync(pathFrom,{'withFileTypes': true})), len_ = ary.length, item; i1 < len_; i1++) {
		item = ary[i1];
		var pathfromitem = ("" + pathFrom + "/" + (item.name));
		var pathfromitemWebp = ("" + pathfromitem + ".webp");
		if (log.pathfromitem.includes(pathfromitem)) { continue; };
		if (!whenState(item,pathFrom)) { log.pathfromitem.push(pathfromitem);continue; };
		if (option.mask && maskFilter(pathfromitem)) { log.pathfromitem.push(pathfromitem);continue; };
		if (krakenUserStatus.sources.length >= option.part) { break; };
		
		// Было ошибкой создание '.webp' сейчас просто прогоняю и исправляю
		// Кракен всего в течении часа сохранет картинки, печально.
		
		if (fs.existsSync(pathfromitemWebp)) {
			if (!getDataKraken(pathfromitem)) { fs.unlinkSync(pathfromitemWebp) || createDataKraken(pathfromitem,true) } else {
				fs.unlinkSync(pathfromitemWebp);
			};
			option.verbose && log.pathfromitem.push(pathfromitem) && log.info(("" + pathfromitem + " - Optimized"));
		} else if (getDataKraken(pathfromitem)) { option.verbose && log.pathfromitem.push(pathfromitem) && log.info(("" + pathfromitem + " : Optimized")) } else {
			krakenUserStatus.sources.push(pathfromitem);
		};
	};
	
	return krakenImagesPart(krakenUserStatus.sources).then(initialPartPictures.bind(this,pathFrom,(part + 1)));
};


// --- Start process --------------------------------

krakenUserStatus().catch(function(e) { return log.error(e); }).then(function(isOk) { return isOk && option.from.forEach(function(item) { return initialPartPictures(item); }); });
